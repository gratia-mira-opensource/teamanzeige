# 1.0.2 05.04.2023
Verbesserung: E-Mailadresse verschlüsselt, bzw. vor Bots geschützt.   
Verbesserung: Layout einfacher anpassen (für Ritz).   
Verbesserung: Einleitungs- und "Abspann"text.   
# 1.0.1 31.10.2022
Verbesserung: Fancyapps App für Vollbildansicht eingebunden.
# 1.0.0 13.10.2022
Erste »stabile« Version.
