# Teamanzeige

Zeigt Kontakte eine Teams an oder kann auch als Bildergalerie genutzt werden.

# Layout hinzufügen
Im Ordner Layout einfach ein bestehende PHP-File kopieren, umbenennen und anpassen. Da die EMailadresse standardmässig geschützt mit JavaScript geschützt wird, wird diese schon fertig ausgeliefert, d.h. muss nicht verlinkt werden. 

# Mögliche CSS Definition

## Für eine Bildergalerie
CSS team-mitarbeiter-Klasse: display: flex; justify-content: center; flex-direction: row; flex-wrap: wrap;   align-items: center;   
CSS team-arbeiter-Klasse: max-width: 49%; padding: 5px;   
CSS team-arbeiterimg-Klasse: height: 400px; object-fit: cover;   
CSS team-arbeiterinfo-Klasse: display: none;   
Eigenes CSS:   

    @media (max-width: 800px) {	
        .team-mitarbeiter {
            flex-direction: column;
        }
        .team-arbeiter {
            max-width: 100%;
        }
        .team-arbeiterimg {
            height: auto;
        }
    }
