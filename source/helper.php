<?php 
 // Kein direkter Aufruf der PHP-Datei
 defined('_JEXEC') or die('Restricted Access');

// Klassen laden
use Joomla\CMS\Factory;

class Teamanzeige {	

    /** @var string Der komplette HTML Code der Teamanzeige. */
    public $Ausgabe; 

    // Die Klasse instanziieren, d.h. ein Objekt erzeugen
    function __construct(&$params) {
        
        $doc = Factory::getDocument();

        $Layout = $params->get('Layout');

        // CSS Definitionen schreiben
        $CSS = '.teamanzeige-unterstrichen {text-decoration: underline;} .teamanzeige-kein-umbruch {white-space: nowrap;}';
        if($params->get('team')) { $CSS .= '.team {' . $params->get('team') . '}'; }
        if($params->get('team-abteilung')) { $CSS .= '.team-abteilung {' . $params->get('team-abteilung') . '}'; }
        if($params->get('team-mitarbeiter')) { $CSS .= '.team-mitarbeiter {' . $params->get('team-mitarbeiter') . '}'; }
        if($params->get('team-arbeiter')) { $CSS .= '.team-arbeiter {' . $params->get('team-arbeiter') . '}'; }
        if($params->get('team-arbeiterimg')) { $CSS .= '.team-arbeiterimg {' . $params->get('team-arbeiterimg') . '}'; }
        if($params->get('team-arbeiterinfo')) { $CSS .= '.team-arbeiterinfo {' . $params->get('team-arbeiterinfo') . '}'; }
        if($params->get('team-arbeiterinfo-link')) { $CSS .= '.team-arbeiterinfo a {' . $params->get('team-arbeiterinfo-link') . '}'; }
        if($params->get('team-arbeiterinfo-link-hover')) { $CSS .= '.team-arbeiterinfo a:hover {' . $params->get('team-arbeiterinfo-link-hover') . '}'; }
        if($params->get('team-mitarbeitername')) { $CSS .= '.team-mitarbeitername {' . $params->get('team-mitarbeitername') . '}'; }
        if($params->get('eigenesCSS')) { $CSS .= $params->get('eigenesCSS'); }
        
        // In Hauptcode schreiben
        if (!empty($CSS)) {
            $doc->addStyleDeclaration($CSS);
        }


        $Ausgabe = '<div class="team">';
        if($params->get('Einleitung')) {
            $Ausgabe .=  $params->get('Einleitung');
        }
        if($params->get('Abteilung1')) {
            $Ausgabe .=  $this->erstelleAusgabeAbteilung($params->get('NameAbteilung1'),$params->get('MitarbeiterAbteilung1'),$Layout);
        }
        if($params->get('Abteilung2')) {
            $Ausgabe .=  $this->erstelleAusgabeAbteilung($params->get('NameAbteilung2'),$params->get('MitarbeiterAbteilung2'),$Layout);
        }
        if($params->get('Abteilung3')) {
            $Ausgabe .=  $this->erstelleAusgabeAbteilung($params->get('NameAbteilung3'),$params->get('MitarbeiterAbteilung3'),$Layout);
        }
        if($params->get('Abteilung4')) {
            $Ausgabe .=  $this->erstelleAusgabeAbteilung($params->get('NameAbteilung4'),$params->get('MitarbeiterAbteilung4'),$Layout);
        }
        if($params->get('Abteilung5')) {
            $Ausgabe .=  $this->erstelleAusgabeAbteilung($params->get('NameAbteilung5'),$params->get('MitarbeiterAbteilung5'),$Layout);
        }
        if($params->get('Abteilung6')) {
            $Ausgabe .=  $this->erstelleAusgabeAbteilung($params->get('NameAbteilung6'),$params->get('MitarbeiterAbteilung6'),$Layout);
        }
        if($params->get('Abspann')) {
            $Ausgabe .=  $params->get('Abspann');
        }
        // Ergebnis bereitstellen
        $this->Ausgabe = $Ausgabe . '</div>';
    }

    // Setzt das Layout zusammen
	public function erstelleAusgabeAbteilung ($Name,$MitarbeiterArray,$Layout) {
        $TelefonSuchen = '/^(\+\d{2})(\d{2})(\d{3})(\d{2})(\d{2,})$/';
        $TelefonErsetzen = '$1 (0) $2 $3 $4 $5';

        /** @var boolean Ist true, wenn es ein Mobilgerät ist */
        $IstMobil = GMF_Layout::istMobil();

        // E-Mail Adressen schützen / vorbereiten
        foreach($MitarbeiterArray as $Mitarbeiter) {
            if($Mitarbeiter->EMail) {
                if($IstMobil) {
                    $Mitarbeiter->EMail = JHtml::_('email.cloak',$Mitarbeiter->EMail,1,'<span class="teamanzeige-unterstrichen">E-Mail</span>');
                } else {
                    $Mitarbeiter->EMail = JHtml::_('email.cloak',$Mitarbeiter->EMail,1,'<span class="teamanzeige-kein-umbruch">'. $Mitarbeiter->EMail .'</span>');
                }
            }
        }
        
        // In diesem File wird der HTMLBlock ($HTMLBlock) generiert.
        include('layout/' . $Layout . '.php');
             
        if(!isset($HTMLBlock)) {
            $HTMLBlock = 'Teamanzeige: Layout nicht bekannt!<br>';
        }
        return $HTMLBlock;
    }
}
?>