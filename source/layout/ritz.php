<?php 
 // Kein direkter Aufruf der PHP-Datei
 defined('_JEXEC') or die('Restricted Access'); 

// Überschreiben der Standartausgabe
$TelefonErsetzen = '0$2 $3 $4 $5';

$HTMLBlock =
    '<div class="team-abteilung">
             <h2>' . $Name . '</h2>
        <div class="team-mitarbeiter">';
        foreach($MitarbeiterArray as $Mitarbeiter) {
$HTMLBlock .= 
            '<div class="team-arbeiter">
                <div class="team-arbeiterbild">
                    <figure>
                        <a data-fancybox="gallery" data-src="' . JUri::base() . $Mitarbeiter->Bild . '" title="' . $Mitarbeiter->Name . '">
                            <img class="team-arbeiterimg bild-' . GMF_Allgemein::unbenennenzuAliasname($Mitarbeiter->Name) . '" src="' . $Mitarbeiter->Bild . '" alt="' . $Mitarbeiter->Name . '" />
                        </a>
                    </figure>
                </div>
                <div class="team-info-block">
					<div class="dreieck-ritz"></div>
					<div class="team-arbeiterinfo">
						<p> <span class="team-mitarbeitername">' . $Mitarbeiter->Name . '</span><br>';
							if($Mitarbeiter->Funktion) {
								$HTMLBlock .= $Mitarbeiter->Funktion . '<br>';
							}
							if($Mitarbeiter->Festnetz) {
								if($IstMobil) {
									$HTMLBlock .= '<a href="tel:' . $Mitarbeiter->Festnetz . '" style="text-decoration: underline;">Telefon</a><br>';
								} else {
									$HTMLBlock .= 'T <a href="tel:' . $Mitarbeiter->Festnetz . '">' . preg_replace($TelefonSuchen,$TelefonErsetzen,$Mitarbeiter->Festnetz) . '</a><br>';
								}
							}
							if($Mitarbeiter->Mobil) {
								if($IstMobil) {
									$HTMLBlock .= '<a href="tel:' . $Mitarbeiter->Mobil . '" style="text-decoration: underline;">Mobile</a><br>';
								} else {
									$HTMLBlock .= 'M <a href="tel:' . $Mitarbeiter->Mobil . '">' . preg_replace($TelefonSuchen,$TelefonErsetzen,$Mitarbeiter->Mobil) . '</a><br>';
								}
							}
							if($Mitarbeiter->EMail) {
								$HTMLBlock .= $Mitarbeiter->EMail . '<br>';
							}
							if($Mitarbeiter->Hinweis) {
								$HTMLBlock .= $Mitarbeiter->Hinweis;
							}
$HTMLBlock .=        '</p>
				</div>
				</div>
            </div>';
        }
$HTMLBlock .=
        '</div>
    </div>';