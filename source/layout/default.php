<?php 
 // Kein direkter Aufruf der PHP-Datei
 defined('_JEXEC') or die('Restricted Access'); 

$HTMLBlock =
    '<div class="team-abteilung">
             <h2>' . $Name . '</h2>
        <div class="com-content-category-blog__items columns-2 team-mitarbeiter">';
        foreach($MitarbeiterArray as $Mitarbeiter) {
$HTMLBlock .=
            '<div class="com-content-category-blog__item blog-item team-arbeiter">
              <figure class="left item-image">
                    <a data-fancybox="gallery" data-src="' . JUri::base() . $Mitarbeiter->Bild . '" title="' . $Mitarbeiter->Name . '">
                        <img class="team-arbeiterimg" src="' . $Mitarbeiter->Bild . '" alt="' . $Mitarbeiter->Name . '" />
                    </a>
                </figure>
                <div class="team-arbeiterinfo">
                    <p> <span class="team-mitarbeitername">' . $Mitarbeiter->Name . '</span><br>';
                            if($Mitarbeiter->Funktion) {
                                $HTMLBlock .= $Mitarbeiter->Funktion . '<br>';
                            }
                            if($Mitarbeiter->Festnetz) {
                                $HTMLBlock .= 'T <a href="tel:' . $Mitarbeiter->Festnetz . '">' . preg_replace($TelefonSuchen,$TelefonErsetzen,$Mitarbeiter->Festnetz) . '</a><br>';
                            }
                            if($Mitarbeiter->Mobil) {
                                $HTMLBlock .= 'M <a href="tel:' . $Mitarbeiter->Mobil . '">' . preg_replace($TelefonSuchen,$TelefonErsetzen,$Mitarbeiter->Mobil) . '</a><br>';
                            }
                            if($Mitarbeiter->EMail) {
                                $HTMLBlock .= $Mitarbeiter->EMail . '<br>';
                            }
                            if($Mitarbeiter->Hinweis) {
                                $HTMLBlock .= $Mitarbeiter->Hinweis;
                            }
$HTMLBlock .=       '</p>
                </div>
            </div>';
        }
$HTMLBlock .=
        '</div>
    </div>';