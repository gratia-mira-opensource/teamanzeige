<?php

// No direct access
defined('_JEXEC') or die;

if(!defined('GMFramework')) {
    exit(   
        'Bitte <a href="https://gitlab.com/gratia-mira-opensource/gratia-mira-framework/-/releases" title="zum Framework" target="_blank">GMFramework installieren</a> oder 
        <a href="https://docs.joomla.org/Help4.x:Plugins:_Name_of_Plugin/de" title="zur Joomla-Hilfeseite" target="_blank">aktivieren</a>!'
    );
}

// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';

$oTeam = new Teamanzeige($params);

require JModuleHelper::getLayoutPath('mod_teamanzeige');
?>