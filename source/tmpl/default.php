<?php
// No direct access
defined('_JEXEC') or die; ?>
<!-- https://fancyapps.com/docs/ui/installation/ -->
<!-- https://codesandbox.io/s/elegant-gates-qwgnd?file=/index.html:781-852 -->
<link
    rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.css"
/>

<?php echo $oTeam->Ausgabe; ?>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>
    <script>
      // Customization example
      Fancybox.bind('[data-fancybox="gallery"]', {
        infinite: false
      });
</script>